package service;

public interface IsekaiService {
    boolean wifeName(Integer number);
    String[] story(Integer chapter);
    String[] chatFromWaifu(Integer chapter);
    void getAllFutureWife();
    String getWifeName();
}
