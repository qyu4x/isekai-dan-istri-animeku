package service;

import repository.IsekaiRepository;

public class IsekaiServiceImpl implements IsekaiService{

    private IsekaiRepository isekaiRepository;

    public IsekaiServiceImpl(IsekaiRepository isekaiRepository) {
        this.isekaiRepository = isekaiRepository;
    }

    @Override
    public boolean wifeName(Integer number) {
        String[] wifes =  isekaiRepository.getAllFutureWife();
        if (wifes[number-1] != null) {
            isekaiRepository.wifeName(wifes[number-1]);
            return true;
        }

        return false;
    }

    @Override
    public String[] story(Integer chapter) {
        if (chapter-1 >= isekaiRepository.getAllStoryData().length) {
            return null;
        }

        return isekaiRepository.story(chapter-1);

    }

    @Override
    public String[] chatFromWaifu(Integer chapter) {
        if (chapter-1 >= isekaiRepository.getAllChatData().length) {
            return null;
        }

        return isekaiRepository.chatFromWaifu(chapter-1);
    }

    @Override
    public void getAllFutureWife() {
        String[] wifes =  isekaiRepository.getAllFutureWife();

        System.out.println("PILIH ISTRI KAMU DULU DONGG");
        // iterate data wifes
        for (int i = 0; i < wifes.length; i++) {
            // check null value
            if (wifes[i] != null) {
                var wife = wifes[i];
                System.out.format("%d. %s \n", i+1, wife);
            }
        }
    }

    @Override
    public String getWifeName() {
        String name = isekaiRepository.getWifeName();
        if (name != null) {
            return name;
        }

        return null;
    }
}
