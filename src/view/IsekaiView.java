package view;

import service.IsekaiService;
import util.InputUtil;

public class IsekaiView {

    IsekaiService isekaiService;
    public IsekaiView(IsekaiService isekaiService) {
        this.isekaiService = isekaiService;
    }

    public void mainViewIsekai() {

        viewChapterOne();
        viewChapterTwo();
        viewChapterThreePart1();
        viewChapterThreePart2();



    }

    public void viewRenderMonotonStoryFromMc(String[] story) {
        for (var i = 0; i < story.length; i++) {
            if (story[i] != null) {
                System.out.println(story[i]);
                var scannerCheck = InputUtil.getInputUser("next y/n -> ");
                if (scannerCheck != null) {
                    continue;
                }
            }
            break;
        }
    }

    public void viewRenderMonotonStoryFromHeroine(String[] story) {
        System.out.println("\n");
        for (var i = 0; i < story.length; i++) {
            if (story[i] != null) {
                System.out.printf("%s(heroine) -> %s \n",isekaiService.getWifeName(), story[i]);
                var scannerCheckChat = InputUtil.getInputUser("Ucok(main character) => ");
                if (scannerCheckChat != null) {
                    continue;
                }
            }
            break;
        }
    }

    public void viewChapterOne() {
        // handle story index 1
        var story = isekaiService.story(1);
        viewRenderMonotonStoryFromMc(story);
    }


    public void viewChapterTwo() {
        // handle choice
        while (true) {
            isekaiService.getAllFutureWife();
            var scanner = InputUtil.getInputUser("TOLONG PILIH ISTRI KAMU DULU");
            var status = isekaiService.wifeName(Integer.parseInt(scanner));
            if (status) {
                break;
            }

            System.out.println("HARUS MILIH ISTRI NGA BOLEH NGA!");
        }
        // handle story index 2
        var story = isekaiService.story(2);
        viewRenderMonotonStoryFromMc(story);

    }

    public void viewChapterThreePart1() {
        // handle chat waifu chaper 3 part 1
        var story = isekaiService.chatFromWaifu(1);
        viewRenderMonotonStoryFromHeroine(story);

        // handle story index 3
        var storyMc = isekaiService.story(3);
        viewRenderMonotonStoryFromMc(storyMc);
    }

    public void viewChapterThreePart2() {
        // handle chat waifu chaper 3 part 2
        var story = isekaiService.chatFromWaifu(2);
        viewRenderMonotonStoryFromHeroine(story);

        // handle story index 4
        var storyMc = isekaiService.story(4);
        viewRenderMonotonStoryFromMc(storyMc);
    }
}
