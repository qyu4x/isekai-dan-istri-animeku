package test.service;

import entity.Isekai;
import repository.IsekaiRepository;
import repository.IsekaiRepositoryImpl;
import service.IsekaiService;
import service.IsekaiServiceImpl;

public class IsekaiServiceTest {

    public static void main(String[] args) {
        testWifeName();
    }

    public static void testWifeName() {
        Isekai isekai = new Isekai();
        IsekaiRepository isekaiRepository = new IsekaiRepositoryImpl(isekai);
        IsekaiService isekaiService = new IsekaiServiceImpl(isekaiRepository);

        isekaiService.getAllFutureWife();
        var result = isekaiService.wifeName(1);
        System.out.println(result);
        System.out.println(isekai.getWifeName());
    }

    public static void testChatFromWaifu() {
        Isekai isekai = new Isekai();
        IsekaiRepository isekaiRepository = new IsekaiRepositoryImpl(isekai);
        IsekaiService isekaiService = new IsekaiServiceImpl(isekaiRepository);

        var results = isekaiService.chatFromWaifu(1);
        for (var result : results) {
            System.out.println(result);
        }
    }

    public static void testStory() {
        Isekai isekai = new Isekai();
        IsekaiRepository isekaiRepository = new IsekaiRepositoryImpl(isekai);
        IsekaiService isekaiService = new IsekaiServiceImpl(isekaiRepository);

        var results = isekaiService.story(2);
        for (var result : results) {
            System.out.println(result);
        }
    }


    public static void testGetAllFutureWife() {
        Isekai isekai = new Isekai();
        IsekaiRepository isekaiRepository = new IsekaiRepositoryImpl(isekai);
        IsekaiService isekaiService = new IsekaiServiceImpl(isekaiRepository);

        isekaiService.getAllFutureWife();
    }
}
