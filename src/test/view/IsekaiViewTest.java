package test.view;

import entity.Isekai;
import repository.IsekaiRepository;
import repository.IsekaiRepositoryImpl;
import service.IsekaiService;
import service.IsekaiServiceImpl;
import view.IsekaiView;

public class IsekaiViewTest {
    public static void main(String[] args) {

        testViewRenderMonotonStoryFromMc();

    }

    public static void testViewRenderMonotonStoryFromMc() {
        Isekai isekai = new Isekai();
        IsekaiRepository isekaiRepository = new IsekaiRepositoryImpl(isekai);
        IsekaiService isekaiService = new IsekaiServiceImpl(isekaiRepository);
        IsekaiView isekaiView = new IsekaiView(isekaiService);


        var story = isekaiService.story(1);
        isekaiView.viewRenderMonotonStoryFromMc(story);
    }
}
