package repository;

import entity.Isekai;

public class IsekaiRepositoryImpl implements IsekaiRepository{
    // dummy data
    private String[][] storyData = {

            {"Konbanwa, namaku adalah ucok, laki-laki berumur 20 tahun..." , "seorang programmer WIBU, jomblo, dengan gaji UMR jogja", "dikejar deadline tidur di kantor itu hal yang biasa..", "saat ini aku sudah tidak tidur 3 hari untuk menyelesaikan project PSE kominfo", "tiba-tiba saat ingin membuat kopi aku terpleset dan kepalaku terbentur meja...", "ahh sialnya...pikirku..dan membayangkan diriku yang sudah mati", "tiba-tiba muncul menu aneh.. Seperti halnya anime SWORD ART ONLINE yang pernah aku lihat"},
            {"Sebernarnya aku tidak tahu untuk apa pilihan itu tadi,,,", "saat aku terbangun, aku sudah tidak ada di kantor lagi..", "melainkan di dunia yang tidak aku kenal di atas tempat tidur seperti layaknya dunia fantasy yang pernah aku lihat di anime", "ISEKAIII?? PIKIRKU SESAAT"},
            {"hahhhh nyataa? jadi nyata gadis anime di anime yang pernah gue tonton jadi nyata??..."},
            {"STRESS ? memang stress wkakwowkw :vv begitulah cerita singkat dari si otong seorang programmer yang terlempar ke isekai"}
    };

    private String [] futureWife = {
        "Kato Megumi", "Mizuhara Chizuru", "Tsukasa Tsukoyomi", "Micchon Shikikimori"
    };

    private String [][] chatData = {
            {"Araa (kaget)... syukurlah, ucok-sama kamu sudah bangun??", "apa kamu baik-baik saja? kamu terlihat kebingungan"},
            {"Aku adalah istimu apakah kamu melupakanku?? baa...k..a (bodoh)", "tapi syukurlah, aku mencemaskanmu..."},

    };

    private Isekai isekai;
    public IsekaiRepositoryImpl(Isekai isekai) {
        this.isekai = isekai;
    }

    @Override
    public void wifeName(String name) {
        isekai.setWifeName(name);
    }

    @Override
    public String[] story(Integer chapter) {
        return storyData[chapter];
    }

    @Override
    public String[] chatFromWaifu(Integer chapter) {
        return chatData[chapter];
    }


    @Override
    public String[][] getAllStoryData() {
        return storyData;
    }

    @Override
    public String[] getAllFutureWife() {
        return futureWife;
    }

    @Override
    public String[][] getAllChatData() {
        return chatData;
    }

    @Override
    public String getWifeName() {
        return isekai.getWifeName();
    }
}
