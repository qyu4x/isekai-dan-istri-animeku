package repository;

public interface IsekaiRepository {
    void wifeName(String name);
    String[] story(Integer chapter);
    String[] chatFromWaifu(Integer chapter);
    String[][] getAllStoryData();
    String[]getAllFutureWife();
    String[][] getAllChatData();
    String getWifeName();
}
