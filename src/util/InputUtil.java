package util;

import java.util.Scanner;

public class InputUtil {

    private static Scanner scanner = new Scanner(System.in);

    public static String getInputUser(String operation) {
        System.out.format("%s ", operation);
        String inputUser = scanner.nextLine();
        return inputUser;
    }
}
