import entity.Isekai;
import repository.IsekaiRepository;
import repository.IsekaiRepositoryImpl;
import service.IsekaiService;
import service.IsekaiServiceImpl;
import view.IsekaiView;

public class IsekaiDanIstriAnimeku {
    public static void main(String[] args) {

        Isekai isekai = new Isekai();
        IsekaiRepository isekaiRepository = new IsekaiRepositoryImpl(isekai);
        IsekaiService isekaiService = new IsekaiServiceImpl(isekaiRepository);
        IsekaiView isekaiView = new IsekaiView(isekaiService);

        isekaiView.mainViewIsekai();
    }
}
